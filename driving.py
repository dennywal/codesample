import sys

## Driver Class
## Initialized with:
##   - Driver name
## Records the trips data for each driver
## total_time method returns the total time elapsed in all trips in hours
## total_distance method returns the total distance elapsed in all trips in miles
## average_speed method returns the averge speed for the driver in miles/hour
class Driver:

    def __init__(self, name):
        self.name = name
        self.trips = []
        self.total_dist = 0
        self.total_time = 0


    def add_trip(self, trip):
        self.trips.append(trip)
        self.total_dist += trip.miles
        self.total_time += trip.time_elapsed()

    def average_speed(self):
        if (self.total_time > 0):
            return self.total_dist / self.total_time
        elif (self.total_dist == 0):
            return 0
        else:
            return None
    

## Trip Class
## Initialized with:
##   - Driver name
##   - Start time (h, m)
##   - End time (h, m)
##   - miles
## Records the trip driver, start and end time, miles driven
## time elapsed method returns the duration of the trip in hours
## Average speed method returns the average speed in miles/hour
class Trip:

    def __init__(self, driver_in, start_time_in, end_time_in, miles_in):
        self.driver = driver_in
        self.start_time = start_time_in
        self.end_time = end_time_in
        self.miles = float(miles_in)

    def time_elapsed(self):
        hours_final = self.end_time[0] + (self.end_time[1] / 60.0)
        hours_initial = self.start_time[0] + (self.start_time[1] / 60.0)

        return hours_final - hours_initial
    
    def average_speed(self):

        if (self.time_elapsed() > 0):
            return self.miles / self.time_elapsed()
        elif (self.miles == 0):
            return 0
        else:
            return None


def parseDriver(line):
    raw_in = line.split(' ')

    return Driver(raw_in[1])

def parseTrip(line):
    raw_in = line.split(' ')
    driver = raw_in[1]
    start_time_raw = raw_in[2].split(':')
    end_time_raw = raw_in[3].split(':')

    start_time = (float(start_time_raw[0]), float(start_time_raw[1]))
    end_time = (float(end_time_raw[0]), float(end_time_raw[1]))

    miles = raw_in[4]

    return(Trip(driver, start_time, end_time, miles))