# Driving History

## General Problem Approach

My approach to this problem was to consider each driver and trip as objects. Using this approach, when reading the input I could first evaluate on each line whether we were adding a new trip or a new driver. I would then store a list of drivers and a list of trips.

I used an object oriented approach to enable this solution to be extensible to further problems which may require the use of driver or trip information (which may be likely to occur). I included test cases to ensure that future modification of these classes does not inadvertently break existing features.

I split the solution into two files, driving.py and main.py. driving.py contains the classes which are used in the solution which main.py contains the problem specific solution implementation. This minimal coupling allows this solution to more easily be extended if the Driver and Trip classes are needed elsewhere since they will not be tied up in the rest of the solution. 

## Trip Class

The trip class stores information about each individual trip. If necessary in the future, this object could be extended to include more functionality. This object must be initialized with each of it's member variables.

### Trip Member Variables

 The trip class has member variables driver, start_time, end_time and miles. The driver variable stores the name of the driver this trip is associated with as a string. The variable start_time stores the start time as an (hours, minutes) tuple. The variable end_time stores the end time as an (hours, minutes) tuple. The variable miles stores the miles driven in the trip as a float.

### Trip Member Functions

 The trip class has member functions time_elapsed and average_speed. The function time_elapsed returns the difference between the end time and the start time of the trip in hours. The function average_speed returns the average speed in miles/hour for the trip. If the total time taken is 0 then None is returned.


## Driver Class

 The driver class stores information about each driver and can be extended if necessary. This  includes the name of the driver and a list of the driver's trips. 

### Driver Member Variables

 The driver class has member variables name, trips, total_dist, and total_time. The variable name is the name of the driver stored as a string. The variable trips is a list of trip objects which each represents a trip of the driver. The variable total_dist stores the total distance the driver travelled over all trips. The variable total_time stores the total time the driver took to complete every trip. 

 I made the decision to store total_dist and total_time rather than use a method to calculate each when they are needed as a precaution to reduce time spent on frequent calculations. Depending on the future application, it may be better not to store this information and only calculate it when it is needed, however, I worked under the assumption that runtime is more important than space in this situation.

### Driver Member Functions

 The driver class has member functions add_trip and average_speed. The add_trip function adds a trip object to the list of trips and adds to the total_dist and total_time variables based on the time and distance calculated from those trips. The average_speed function calculates the average speed in miles/hour over all trips. If the total time taken is 0, then None is returned.

## Testing

 I used a combination of unit tests and integration tests to ensure that each aspect of my solution is working individually and in combination with the rest of the solution. I tested for some potential edge cases which could cause the solution to fail. I ran these tests with pytest, however they could be used with another testing process. 

 I split the testing into two files. One file tests the two main classes stored in driving.py and the other file tests the main driver function in main.py. The test_main.py file contains the integration tests including a test of the example input and output. These integration test files are included with the rest of the solution. 