import sys
import driving

## Read in file
## Parse each driver and trip and store each in a list
def parseInput(input_file):
    Drivers = set()
    Trips = []

    with open(input_file) as f:
        content = f.read().splitlines()
        for line in content:
            if line.split(' ')[0] == 'Driver':
                Drivers.add(driving.parseDriver(line))
            elif line.split(' ')[0] == 'Trip':
                Trips.append(driving.parseTrip(line))

    return (Drivers, Trips)

## Adds each trip to the correct driver
def addTrips(Drivers, Trips):
    for driver in Drivers:
        for trip in Trips:
            if trip.driver == driver.name:
                driver.add_trip(trip)

    Drivers = sorted(Drivers, key=lambda d: d.total_dist, reverse=True)

    return Drivers

## Puts data in correct output format
def formatOutput(Drivers, out=sys.stdout):
    for driver in Drivers:

        name = driver.name
        distance_out = round(driver.total_dist)
        speed_out = round(driver.average_speed())

        output =  name + ': ' + str(distance_out) + ' miles'
        if distance_out > 0:
            output += ' @ ' + str(speed_out) + ' mph'

        if ((speed_out >= 5) and speed_out < 100) or (distance_out == 0):
            out.write(output)
            out.write('\n')
    
    return out

def main():
    input_file = sys.argv[1]

    Drivers, Trips = parseInput(input_file)
    Drivers = addTrips(Drivers, Trips)
    formatOutput(Drivers)


if __name__ == '__main__':
    main()