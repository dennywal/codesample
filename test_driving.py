import driving

## Tests that drivers are initialized correctly
def test_driver_init():
    test_driver = driving.Driver('test_name')

    assert test_driver.name == 'test_name'

## Tests that trips are initialized correctly
def test_trip_init():
    test_trip = driving.Trip('test_name', (4, 50), (6, 2), 28)

    assert test_trip.driver == 'test_name'
    assert test_trip.start_time == (4, 50)
    assert test_trip.end_time == (6, 2)
    assert test_trip.miles == 28

## Tests that drivers are parsed properly
def test_parse_driver():
    line = 'Driver testName'

    test_driver = driving.parseDriver(line)

    assert test_driver.name == 'testName'

## Tests that trips are parsed properly
def test_parse_trip():
    line = 'Trip testName 04:02 06:02 40'

    test_trip = driving.parseTrip(line)

    assert test_trip.driver == 'testName'
    assert test_trip.start_time == (4, 2)
    assert test_trip.end_time == (6, 2)
    assert test_trip.miles == 40

## Tests that time elapsed is calculated correctly
def test_trip_time_elapsed():
    line = 'Trip testName 04:02 06:02 40'
    test_trip = driving.parseTrip(line)

    assert test_trip.time_elapsed() == 2

## Tests that the trip time converts minutes to
## hours correctly
def test_trip_time_elapsed_minutes():
    line = 'Trip testName 04:00 06:15 40'
    test_trip = driving.parseTrip(line)

    assert test_trip.time_elapsed() == 2.25

## Tests that the average speed is calculated correctly
def test_trip_average_speed():
    line = 'Trip testName 04:02 06:17 40'
    test_trip = driving.parseTrip(line)

    assert round(test_trip.average_speed(), 4) == 17.7778

## Tests that 0 denominators get handled by recording an
## average speed of None
def test_trip_average_speed_time_is_zero():
    line = 'Trip testName 04:02 04:02 40'
    test_trip = driving.parseTrip(line)

    assert test_trip.average_speed() == None

## Tests that the trip object is added correctly with add_trip
def test_driver_add_trip():
    line1 = 'Trip testName 04:02 06:02 40'
    line3 = 'Driver testName'
    test_trip1 = driving.parseTrip(line1)
    test_driver = driving.parseDriver(line3)

    test_driver.add_trip(test_trip1)

    assert test_driver.trips[0].driver == 'testName'
    assert test_driver.trips[0].start_time == (4, 2)
    assert test_driver.trips[0].end_time == (6, 2)
    
## Tests that two trip objects can be added to
## a driver object instance
def test_driver_add_two_trips():
    line1 = 'Trip testName 04:02 06:02 40'
    line2 = 'Trip testName 02:02 06:02 80'
    line3 = 'Driver testName'
    test_trip1 = driving.parseTrip(line1)
    test_trip2 = driving.parseTrip(line2)
    test_driver = driving.parseDriver(line3)

    test_driver.add_trip(test_trip1)
    test_driver.add_trip(test_trip2)

    assert test_driver.total_dist == 120
    assert test_driver.total_time == 6

## Tests that the driver speed is averaged correctly
def test_driver_average_speed_two_trips():
    line1 = 'Trip testName 04:02 06:02 40'
    line2 = 'Trip testName 02:02 06:02 160'
    line3 = 'Driver testName'
    test_trip1 = driving.parseTrip(line1)
    test_trip2 = driving.parseTrip(line2)
    test_driver = driving.parseDriver(line3)

    test_driver.add_trip(test_trip1)
    test_driver.add_trip(test_trip2)

    assert round(test_driver.average_speed(), 4) == 33.3333