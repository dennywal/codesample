import driving
import main
import sys

## Tests that trips are correctly added to the driver object
def test_add_trips_to_drivers():
    line1 = 'Trip testName 04:02 06:02 40'
    line2 = 'Trip testName 02:02 06:02 160'
    line3 = 'Driver testName'
    test_trip1 = driving.parseTrip(line1)
    test_trip2 = driving.parseTrip(line2)
    test_driver = driving.parseDriver(line3)

    Drivers = [test_driver]
    Trips = [test_trip1, test_trip2]

    new_drivers = main.addTrips(Drivers, Trips)

    assert new_drivers[0].trips[0].miles == 40
    assert new_drivers[0].trips[1].miles == 160


## Tests that drivers are properly ordered after trips 
## have been added
def test_add_trips_to_multiple_drivers_with_correct_order():
    line1 = 'Trip testName 04:02 06:02 40'
    line2 = 'Trip testName 02:02 06:02 160'
    line3 = 'Driver testName1'
    line4 = 'Driver testName2'
    line5 = 'Trip testName2 01:00 02:00 1000'
    line6 = 'Trip testName2 05:00 07:00 100'
    test_trip1 = driving.parseTrip(line1)
    test_trip2 = driving.parseTrip(line2)
    test_driver1 = driving.parseDriver(line3)
    test_driver2 = driving.parseDriver(line4)
    test_trip3 = driving.parseTrip(line5)
    test_trip4 = driving.parseTrip(line6)

    Drivers = [test_driver1, test_driver2]
    Trips = [test_trip1, test_trip2, test_trip3, test_trip4]

    new_drivers = main.addTrips(Drivers, Trips)

    assert new_drivers[0].name == 'testName2'
    assert new_drivers[1].name == 'testName1'


## Tests that final output is formatted correctly and 
## includes the correct drivers
def test_format_output_prints_correctly():
    from io import StringIO

    line1 = 'Trip testName1 04:02 06:02 40'
    line2 = 'Trip testName1 02:02 06:02 160'
    line3 = 'Driver testName1'
    line4 = 'Driver testName2'
    line5 = 'Trip testName2 01:00 02:00 1000'
    line6 = 'Trip testName2 05:00 07:00 100'
    test_trip1 = driving.parseTrip(line1)
    test_trip2 = driving.parseTrip(line2)
    test_driver1 = driving.parseDriver(line3)
    test_driver2 = driving.parseDriver(line4)
    test_trip3 = driving.parseTrip(line5)
    test_trip4 = driving.parseTrip(line6)

    Drivers = [test_driver1, test_driver2]
    Trips = [test_trip1, test_trip2, test_trip3, test_trip4]
    new_drivers = main.addTrips(Drivers, Trips)

    test_out = StringIO()
    test_out = main.formatOutput(new_drivers, out=test_out)

    output = test_out.getvalue().strip()

    test_correct_file = open('test_output_correct.txt', 'r')

    output_correct = test_correct_file.read()

    assert output == output_correct

## Tests the example case
def test_example_case():
    from io import StringIO

    drivers, trips = main.parseInput('example_input.txt')
    drivers = main.addTrips(drivers, trips)

    test_out = StringIO()
    test_out = main.formatOutput(drivers, out=test_out)

    output = test_out.getvalue().strip()

    correct_file = open('example_output.txt', 'r')
    output_correct = correct_file.read()

    assert output == output_correct